Meteor.startup(function() {
	Numbers.remove({});
	Numbers.insert({
		value: 0
	});

	Numbers.allow({
		update: function(userId, doc) {
			return true;
		}
	});
});

Meteor.publish('all-numbers', function() {
	return Numbers.find({});
});