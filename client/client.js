Meteor.subscribe('all-numbers');

Meteor.startup(function() {

});

Template.test.number = function() {
	return Numbers.findOne({});
};

Template.test.events({
	'click #button': function(event, instance) {
		var currentNumber = Numbers.findOne({});
		if(!currentNumber) return;

		Numbers.update({_id: currentNumber._id}, {$inc: {value: 1}});
	}
});